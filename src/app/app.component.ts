import * as d3 from 'd3';
import { Component, AfterContentInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterContentInit {
  title = 'app';


  radius = 10;

  ngAfterContentInit() {
    d3.select('p').style('color', 'red');
    this.colorMe();
  }

  colorMe() {
    d3.select('button').style('color', 'red');
  }

  makeRadius(event: any) {
    console.log(event);
    
    d3.select(event.target).append('circle')
      .attr('cx', event.x)
      .attr('cy', event.y)
      .attr('r', () => {
        return this.radius;
      })
      .attr('fill', 'red');
  }
}
